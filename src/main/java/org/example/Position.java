package org.example;

public class Position {
    int xCoordinate;
    int yCoordinate;
    Direction direction;
    public Position(int xCoordinate, int yCoordinate, Direction direction) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Position position = (Position) object;

        if (xCoordinate != position.xCoordinate) return false;
        if (yCoordinate != position.yCoordinate) return false;
        return direction == position.direction;
    }

    @Override
    public int hashCode() {
        int result = xCoordinate;
        result = 31 * result + yCoordinate;
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        return result;
    }
}
