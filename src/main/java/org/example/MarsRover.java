package org.example;

public class MarsRover {
    Position position;
    public MarsRover(Position position) {
        this.position = position;
    }

    public Position move(Position position) {
        if(position.direction == Direction.North) {
            this.position = new Position(position.xCoordinate, position.yCoordinate + 1, position.direction);
        }
        else if(position.direction == Direction.South) {
            this.position = new Position(position.xCoordinate, position.yCoordinate - 1, position.direction);
        }
        else if(position.direction == Direction.East) {
            this.position = new Position(position.xCoordinate + 1, position.yCoordinate, position.direction);
        }
        else {
            this.position = new Position(position.xCoordinate - 1, position.yCoordinate, position.direction);
        }
        return this.position;
    }

    public Position turnRight(Position position) {
        if(position.direction == Direction.North) {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.East);
        }
        else if(position.direction == Direction.South) {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.West);
        }
        else if(position.direction == Direction.East) {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.South);
        }
        else {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.North);
        }
        return this.position;
    }

    public Position turnLeft(Position position) {
        if(position.direction == Direction.North) {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.West);
        }
        else if(position.direction == Direction.South) {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.East);
        }
        else if(position.direction == Direction.East) {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.North);
        }
        else {
            this.position = new Position(position.xCoordinate, position.yCoordinate, Direction.South);
        }
        return this.position;
    }

    public Position getPosition() {
        return position;
    }
}
