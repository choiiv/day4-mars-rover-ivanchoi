package org.example;

public class ControlProgram {
    MarsRover marsRover;
    public ControlProgram(MarsRover marsRover) {
        this.marsRover = marsRover;
    }

    public String executeCommands(String commands) {
        Position currentPosition = marsRover.getPosition();
        for (int i = 0; i < commands.length(); i++) {
            if(commands.charAt(i) == 'M') {
                currentPosition = marsRover.move(currentPosition);
            }
            if(commands.charAt(i) == 'R') {
                currentPosition = marsRover.turnRight(currentPosition);
            }
            if(commands.charAt(i) == 'L') {
                currentPosition = marsRover.turnLeft(currentPosition);
            }
        }
        return String.format("%d %d %s",
                currentPosition.xCoordinate,
                currentPosition.yCoordinate,
                currentPosition.direction.toString().charAt(0));
    }
}
