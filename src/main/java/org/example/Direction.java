package org.example;

public enum Direction {
    North,
    South,
    East,
    West
}
