package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {
    @Test
    public void should_return_y_coordinate_1_when_mars_rover_move_given_initial_coordinate_facing_north() {
        // given
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.move(position);
        // then
        Position expectedPosition = new Position(0, 1, Direction.North);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_y_coordinate_minus_1_when_mars_rover_move_given_initial_coordinate_facing_south() {
        // given
        Position position = new Position(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.move(position);
        // then
        Position expectedPosition = new Position(0, -1, Direction.South);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_x_coordinate_1_when_mars_rover_move_given_initial_coordinate_facing_east() {
        // given
        Position position = new Position(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.move(position);
        // then
        Position expectedPosition = new Position(1, 0, Direction.East);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_x_coordinate_minus_1_when_mars_rover_move_given_initial_coordinate_facing_west() {
        // given
        Position position = new Position(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.move(position);
        // then
        Position expectedPosition = new Position(-1, 0, Direction.West);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_east_when_mars_rover_turn_right_given_initial_coordinate_facing_north() {
        // given
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnRight(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.East);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_south_when_mars_rover_turn_right_given_initial_coordinate_facing_east() {
        // given
        Position position = new Position(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnRight(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.South);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_west_when_mars_rover_turn_right_given_initial_coordinate_facing_south() {
        // given
        Position position = new Position(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnRight(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.West);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_north_when_mars_rover_turn_right_given_initial_coordinate_facing_west() {
        // given
        Position position = new Position(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnRight(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.North);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_west_when_mars_rover_turn_left_given_initial_coordinate_facing_north() {
        // given
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnLeft(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.West);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_north_when_mars_rover_turn_left_given_initial_coordinate_facing_east() {
        // given
        Position position = new Position(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnLeft(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.North);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_east_when_mars_rover_turn_left_given_initial_coordinate_facing_south() {
        // given
        Position position = new Position(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnLeft(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.East);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }

    @Test
    public void should_return_south_when_mars_rover_turn_left_given_initial_coordinate_facing_west() {
        // given
        Position position = new Position(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(position);
        // when
        Position actualPosition = marsRover.turnLeft(position);
        // then
        Position expectedPosition = new Position(0, 0, Direction.South);
        Assertions.assertEquals(expectedPosition, actualPosition);
    }
}
