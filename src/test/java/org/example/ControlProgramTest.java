package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ControlProgramTest {
    @Test
    public void should_return_y_coordinate_1_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_move_command() {
        // given
        String command = "M";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        String actual = controlProgram.executeCommands(command);
        // then
        String expected = "0 1 N";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_return_east_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_turn_right_command() {
        // given
        String command = "R";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        String actual = controlProgram.executeCommands(command);
        // then
        String expected = "0 0 E";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_return_west_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_turn_left_command() {
        // given
        String command = "L";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        String actual = controlProgram.executeCommands(command);
        // then
        String expected = "0 0 W";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_return_x_coordinate_minus_1_and_west_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_turn_left_and_move_command() {
        // given
        String command = "LM";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        String actual = controlProgram.executeCommands(command);
        // then
        String expected = "-1 0 W";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_return_x_coordinate_1_and_east_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_turn_right_and_move_command() {
        // given
        String command = "RM";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        String actual = controlProgram.executeCommands(command);
        // then
        String expected = "1 0 E";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_return_north_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_turn_right_and_turn_left_command() {
        // given
        String command = "RL";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        String actual = controlProgram.executeCommands(command);
        // then
        String expected = "0 0 N";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_return_x_coordinate_1_and_y_coordinate_1_and_north_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_R_M_L_M_command() {
        // given
        String command = "RMLM";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        String actual = controlProgram.executeCommands(command);
        // then
        String expected = "1 1 N";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void should_return_x_coordinate_1_and_y_coordinate_1_and_north_when_control_program_execute_commands_given_rover_initial_coordinate_facing_north_and_R_M_command_followed_by_L_M_command() {
        // given
        String command = "RM";
        String command2 = "LM";
        Position position = new Position(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(position);
        ControlProgram controlProgram = new ControlProgram(marsRover);
        // when
        controlProgram.executeCommands(command);
        String actual = controlProgram.executeCommands(command2);
        // then
        String expected = "1 1 N";
        Assertions.assertEquals(expected, actual);
    }
}
